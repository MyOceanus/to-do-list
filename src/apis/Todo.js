import axios from "axios";

const instance = axios.create(
    {
        // baseURL: 'https://64c0b66b0d8e251fd11264b5.mockapi.io/api/'
        baseURL: 'http://localhost:8080/'
    }
)

export const getTodos = () => {
    return instance.get('/todos')
}

export const createTodos = (text) => {
    return instance.post('/todos', {
        done: false,
        text
    })
}

export const deleteTodos = (id) => {
    return instance.delete(`/todos/${id}`)
}

export const updateTodos = (item) => {
    return instance.put(`/todos/done/${item.id}`)
}

export const updateTodosText = (item) => {
    return instance.put(`/todos/text/${item.id}`, {
        text: item.text
    })
}

export const getTodosById = (id) => {
    return instance.get(`/todos/${id}`)
}