import { useParams } from "react-router-dom"
import React, { useState } from 'react';
import useTodos from "../hooks/useTodos";

const DoneDetail = () => {
    const { id } = useParams()

    const [text, setText] = useState();

    const { getById } = useTodos()

    getById(id).then(data => {
        const text = data.data.text
        setText(text)
    }).catch(error => {
            
    });

    return (
        <div>
            <div>{id}</div>
            <div>{text}</div>
        </div>
    )
}
export default DoneDetail