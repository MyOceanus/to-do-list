import { useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { Divider, List } from "antd"

const DoneList = () => {



    const doneList = useSelector(state => state.todoList.todolist)
        .filter(item => item.done)

    const navigate = useNavigate()
    const handleClickDone = (id) => {
        navigate(`/doneDetail/${id}`)
    }

    return (


        <div>
            <Divider orientation="left"></Divider>
            <List
                style={{ width: "500px", margin: "auto" }}
                size="small"
                header={<div></div>}
                footer={<div></div>}
                bordered
                dataSource={doneList}
                renderItem={(item) => (
                    <List.Item>
                        <div onClick={() => handleClickDone(item.id)}>
                            {item.text}
                        </div>
                    </List.Item>
                )}
            />
        </div>

    )
    //     <div>
    //         {
    //             doneList.map(doneListItem => {
    //                 return (
    //                     <div onClick={() => handleClickDone(doneListItem.id)}>
    //                         <input key={doneListItem.id} value={doneListItem.text} disabled></input>
    //                     </div>
    //                 )
    //             })
    //         }
    //     </div>
    // )
}

export default DoneList