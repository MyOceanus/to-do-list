import { configureStore } from '@reduxjs/toolkit'
import todoListReducer from '../components/TodoListSlice'

export default configureStore({
  reducer: {
    todoList: todoListReducer,
},
})