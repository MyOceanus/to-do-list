import { useDispatch } from "react-redux"
import { uploadTodolist } from "../components/TodoListSlice"
import { createTodos, deleteTodos, getTodos, getTodosById, updateTodos, updateTodosText } from './../apis/Todo';

const useTodos = () => {
    const dispatch = useDispatch()
    const get = async () => {
        const { data } = await getTodos()
        dispatch(uploadTodolist(data))
    }

    const deletes = async (id) => {
        await deleteTodos(id)
        get()
    }

    const create = async (text) => {
        await createTodos(text)
        get()
    }

    const update = async (item) => {
        await updateTodos(item)
        get()
    }

    const updateText = async (item) => {
        await updateTodosText(item)
        get()
    }

    const getById = async (id) => {
        return await getTodosById(id)

    }

    return {
        get,
        deletes,
        create,
        update,
        updateText,
        getById,
    }
}

export default useTodos