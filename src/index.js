import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './app/store'
import { Provider } from 'react-redux'
import { createBrowserRouter,RouterProvider } from 'react-router-dom';
import HelpPage from './pages/HelpPage';
import NotFound from './pages/NotFound';
import DoneList from './pages/DoneList';
import TodoList from './components/TodoList';
import DoneDetail from './pages/DoneDetail';

const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
  {
    path: "/",
    element: <App></App>,
    children: [
      {
        index: true,
        element: <TodoList></TodoList>
      },
      {
        path: "/help",
        element: <HelpPage></HelpPage>
      },
      {
        path: "/done",
        element: <DoneList></DoneList>
      },
      {
        path: "/doneDetail/:id",
        element: <DoneDetail></DoneDetail>
      }
    ]
  },
  {
    path: "*",
    element: <NotFound></NotFound>
  }
])
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router}></RouterProvider>
    </Provider>
  </React.StrictMode>
);

reportWebVitals();
