import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'todoList',
  initialState: {
    todolist: [],
  },
  reducers: {
    uploadTodolist: (state, action) => {
      state.todolist = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { uploadTodolist } = counterSlice.actions

export default counterSlice.reducer