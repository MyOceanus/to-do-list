import { useSelector } from "react-redux"
import "./TodoItem.css"
import useTodos from "../hooks/useTodos"
import { useState } from "react"
import { Button, Modal, Popconfirm, Divider, List } from "antd"
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import TextArea from "antd/es/input/TextArea"
const TodoItem = () => {

    const { deletes } = useTodos()
    const { update } = useTodos()
    const { updateText } = useTodos()

    const [showInput, setShowInput] = useState(false);
    const [confirmLoading] = useState(false);
    const [text, setText] = useState('');
    const [eItem, setEItem] = useState(null);

    const todolists = useSelector(state => state.todoList.todolist)

    const handleChangeDone = (item) => {
        update(item)
    }
    const handleMoveTodolist = (id) => {
        deletes(id)
    }

    const handleSave = () => {
        updateText({
            id: eItem.id,
            text: text,
            done: false
        })
        setShowInput(false);
    };

    const handleEdit = (item) => {
        setShowInput(true)
        setEItem(item)
    };


    const handleCancel = () => {
        setText('');
        setShowInput(false);
    };

    return (
        <div>
            <Divider orientation="left"></Divider>
            <List
                style={{ width: "500px", margin: "auto" }}
                size="small"
                header={<div></div>}
                footer={<div></div>}
                bordered
                dataSource={todolists}
                renderItem={(item) => (
                    <List.Item
                        actions={[
                            <Popconfirm
                                title="Delete the task"
                                description="Are you sure to delete this task?"
                                onConfirm={() => handleMoveTodolist(item.id)}
                                okText="Yes"
                                cancelText="No"
                            >
                                <Button icon={<DeleteOutlined />} size={20}></Button>
                            </Popconfirm>,
                            <Button onClick={() => handleEdit(item)} size={20} icon={<EditOutlined />}>edit</Button>
                        ]}
                    >
                        <div onClick={() => handleChangeDone(item)}
                            style={(item.done) ? { textDecoration: 'line-through' } : { textDecoration: 'none' }}>
                            {item.text}

                        </div>
                    </List.Item>
                )}
            />


            <Modal
                title="please edit content"
                open={showInput}
                onOk={handleSave}
                confirmLoading={confirmLoading}
                onCancel={handleCancel}
            >
                <TextArea rows={4} placeholder="maxLength is 60" maxLength={6} value={text} onChange={event => setText(event.target.value)} />
            </Modal>


        </div>
    )
}
export default TodoItem



