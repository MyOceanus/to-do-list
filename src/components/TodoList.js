import { useEffect } from "react"
import Todogenerator from "./Todogenerator"
import useTodos from "../hooks/useTodos"
import TodoItem from "./TodoItem"
const TodoList = () => {

    const { get } = useTodos()

    useEffect(() => {
        get()
    }, [get])

    return (
        <div>
            <h3>To Do List</h3>
            <TodoItem></TodoItem>
            <Todogenerator></Todogenerator>
        </div>
    )
}

export default TodoList