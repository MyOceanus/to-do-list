import { Link } from "react-router-dom"
import React, { useState } from 'react';
import { MailOutlined } from '@ant-design/icons';
import { Menu } from 'antd';


const items = [
  {
    label: (
      <Link to="/">Home page</Link>
    ),
    key: 'mail',
    icon: <MailOutlined />,
  },
  {
    label: (
      <Link to="/help">Help page</Link>
    ),
    key: 'alipay',
  },
  {
    label: (
      <Link to="/done">Done page</Link>
    ),
    key: 'pay',
  },
];

const Navigate = () => {

  const [current, setCurrent] = useState('mail');
  const onClick = (e) => {
    setCurrent(e.key);
  };
  return <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items} />;
}
export default Navigate