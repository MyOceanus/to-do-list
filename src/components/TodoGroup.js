import { useSelector } from "react-redux"
import TodoItem from "./TodoItem"

const TodoGroup = () =>{

    const todolist = useSelector(state => state.todoList.todolist)

    return(
        <div>
            {
                todolist.map((value,index)=><TodoItem todoStr={value.text} key={index} id={value.id} todoitem ={value.done}></TodoItem>)
            }
        </div>
    )

}

export default TodoGroup