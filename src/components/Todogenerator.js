import { useState } from "react"
import useTodos from "../hooks/useTodos";
import { Button, Input } from "antd";
import { FormOutlined } from '@ant-design/icons';

const Todogenerator = () => {

    const [inputStr, setInputStr] = useState("")
    const {create} = useTodos()

    const handleChangeInputStr = (event) => {
        setInputStr(event.target.value)
    }

    const handleChange =  () => {
        if (!inputStr) {
            alert("please input")
        }
        else {
            create(inputStr)
        }
    }
    return (
        <div>
            <Input showCount maxLength={100} style={{width: 500,margin: 10}} onChange={handleChangeInputStr} value={inputStr}></Input>
            <Button onClick={handleChange} size={20} icon={<FormOutlined />}>add</Button>
        </div>
    )
}

export default Todogenerator